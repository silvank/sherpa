#ifndef COMIX_Main_Comix_H
#define COMIX_Main_Comix_H

#include "COMIX/Main/Process_Group.H"
#include "COMIX/Amplitude/Amplitude.H"
#include "PHASIC++/Main/Process_Integrator.H"
#include "PHASIC++/Process/ME_Generator_Base.H"
#include "ATOOLS/Org/My_MPI.H"

namespace MODEL  { class Model_Base;   }
namespace PDF    { class Remnant_Base; }

namespace COMIX {

  class Single_Process;

  class Comix: public Process_Group, public PHASIC::ME_Generator_Base {
  private :

    std::vector<std::vector<Single_Process*> > m_umprocs;
    std::vector<PHASIC::Process_Base*>         m_rsprocs;

    std::string m_path, m_file;
    time_t m_mets;

#ifdef USING__Threading
    CDBG_ME_TID_Vector m_cts;
#endif

    void PrintLogo(std::ostream &s);
    void PrintVertices();

  public :

    // constructor
    Comix();

    // destructor
    ~Comix();

    // member functions
    bool Initialize(const std::string &path,const std::string &file,
                    MODEL::Model_Base *const model,
                    BEAM::Beam_Spectra_Handler *const beamhandler,
                    PDF::ISR_Handler *const isrhandler);
    PHASIC::Process_Base *InitializeProcess(const PHASIC::Process_Info &pi,
                                            bool add);
    int PerformTests();
    bool NewLibraries();

  }; // end of class Comix

} // end of namespace COMIX

#endif

#include "COMIX/Main/Single_Process.H"
#include "COMIX/Main/Single_Dipole_Term.H"
#include "PDF/Main/ISR_Handler.H"
#include "ATOOLS/Org/Run_Parameter.H"
#include "ATOOLS/Math/Random.H"
#include "ATOOLS/Org/MyStrStream.H"
#include "ATOOLS/Org/Default_Reader.H"
#include "MODEL/Main/Model_Base.H"
#include "PDF/Remnant/Remnant_Base.H"
#include "PHASIC++/Main/Phase_Space_Handler.H"
#include "METOOLS/Explicit/Vertex.H"
#include "ATOOLS/Org/Shell_Tools.H"
#include "ATOOLS/Org/My_File.H"
#include "ATOOLS/Org/CXXFLAGS.H"

using namespace COMIX;
using namespace PHASIC;
using namespace MODEL;
using namespace ATOOLS;

Comix::Comix(): 
  ME_Generator_Base("Comix")
{
#ifdef USING__Threading
  p_cts=&m_cts;
#endif
}

Comix::~Comix() 
{
#ifdef USING__Threading
  for (size_t i(0);i<m_cts.size();++i) {
    CDBG_ME_TID *tid(m_cts[i]);
    tid->m_s=0;
    pthread_cond_wait(&tid->m_s_cnd,&tid->m_s_mtx);
    int tec(0);
    if ((tec=pthread_join(tid->m_id,NULL)))
      THROW(fatal_error,"Cannot join thread"+ToString(i));
    pthread_mutex_unlock(&tid->m_t_mtx);
    pthread_mutex_destroy(&tid->m_t_mtx);
    pthread_mutex_destroy(&tid->m_s_mtx);
    pthread_cond_destroy(&tid->m_t_cnd);
    pthread_cond_destroy(&tid->m_s_cnd);
  }
#endif
}

#define RED(ARG) om::red<<ARG<<om::reset
#define GREEN(ARG) om::green<<ARG<<om::reset
#define BLUE(ARG) om::blue<<ARG<<om::reset
#define YELLOW(ARG) om::brown<<ARG<<om::reset
#define BLACK(ARG) ARG

void Comix::PrintLogo(std::ostream &s)
{
  s<<"+----------------------------------+\n";
  s<<"|                                  |\n";
  s<<"|      "<<RED("CCC")<<"  "<<GREEN("OOO")<<"  "
   <<BLUE("M")<<"   "<<BLUE("M")<<" "<<BLACK("I")<<" "
   <<YELLOW("X")<<"   "<<YELLOW("X")<<"     |\n";
  s<<"|     "<<RED("C")<<"    "<<GREEN("O")<<"   "
   <<GREEN("O")<<" "<<BLUE("MM")<<" "<<BLUE("MM")
   <<" "<<BLACK("I")<<"  "<<YELLOW("X")<<" "
   <<YELLOW("X")<<"      |\n";
  s<<"|     "<<RED("C")<<"    "<<GREEN("O")
   <<"   "<<GREEN("O")<<" "<<BLUE("M")<<" "
   <<BLUE("M")<<" "<<BLUE("M")<<" "<<BLACK("I")
   <<"   "<<YELLOW("X")<<"       |\n";
  s<<"|     "<<RED("C")<<"    "<<GREEN("O")
   <<"   "<<GREEN("O")<<" "<<BLUE("M")<<"   "
   <<BLUE("M")<<" "<<BLACK("I")<<"  "
   <<YELLOW("X")<<" "<<YELLOW("X")<<"      |\n";
  s<<"|      "<<RED("CCC")<<"  "<<GREEN("OOO")
   <<"  "<<BLUE("M")<<"   "<<BLUE("M")<<" "
   <<BLACK("I")<<" "<<YELLOW("X")<<"   "
   <<YELLOW("X")<<"     |\n";
  s<<"|                                  |\n";
  s<<"+==================================+\n";
  s<<"|  Color dressed  Matrix Elements  |\n";
  s<<"|     http://comix.freacafe.de     |\n";
  s<<"|   please cite  JHEP12(2008)039   |\n";
  s<<"+----------------------------------+\n";
#ifdef USING__Threading
  s<<"Comix was compiled with thread support.\n";
#endif
  rpa->gen.AddCitation
    (1,"Comix is published under \\cite{Gleisberg:2008fv}.");
}

void Comix::PrintVertices()
{
  if (msg_LevelIsDebugging()) {
    msg_Out()<<METHOD<<"(): {\n\n   Implemented currents:\n\n";
    Current_Getter::PrintGetterInfo(msg_Out(),10);
    msg_Out()<<"\n   Implemented lorentz calculators:\n\n";
    LC_Getter::PrintGetterInfo(msg_Out(),10);
    msg_Out()<<"\n   Implemented color calculators:\n\n";
    CC_Getter::PrintGetterInfo(msg_Out(),10);
    msg_Out()<<"\n}\n";
  }
}

bool Comix::Initialize(const std::string &path,const std::string &file,
		       MODEL::Model_Base *const model,
		       BEAM::Beam_Spectra_Handler *const beamhandler,
		       PDF::ISR_Handler *const isrhandler) 
{
  m_path=path;
  m_file=file;
  p_model=model;
  p_int->SetBeam(beamhandler); 
  p_int->SetISR(isrhandler);
  // init mapping file
  Default_Reader reader;
  reader.SetInputPath(m_path);
  reader.SetInputFile(m_file);
  SetPSMasses(&reader);
  s_partcommit=reader.Get<int>("COMIX_PARTIAL_COMMIT",0);
  PrintLogo(msg->Info());
  PrintVertices();
  rpa->gen.SetVariable
    ("COMIX_PMODE",reader.Get<std::string>("COMIX_PMODE","D"));

  int helpi;

  helpi = reader.Get("COMIX_WF_MODE", 0, "wave function mode", METHOD);
  rpa->gen.SetVariable("COMIX_WF_MODE",ToString(helpi));

  helpi = reader.Get("COMIX_PG_MODE", 0, "print graph mode", METHOD);
  rpa->gen.SetVariable("COMIX_PG_MODE",ToString(helpi));

  helpi = reader.Get("COMIX_VL_MODE", 0, "vertex label mode", METHOD);
  Vertex::SetVLMode(helpi);

  helpi = reader.Get("COMIX_N_GPL", 3, "graphs per line", METHOD);
  rpa->gen.SetVariable("COMIX_N_GPL",ToString(helpi));

  double helpd;

  helpd = reader.Get("DIPOLE_AMIN", 1.0e-8, "dipole \\alpha_{cut}", METHOD);
  rpa->gen.SetVariable("DIPOLE_AMIN",ToString(helpd));

  helpd = reader.Get("DIPOLE_ALPHA", 1.0, "dipole \\alpha_{max}", METHOD);
  rpa->gen.SetVariable("DIPOLE_ALPHA",ToString(helpd));

  helpd = reader.Get("DIPOLE_ALPHA_FF", 0.0, "FF dipole \\alpha_{max}", METHOD);
  rpa->gen.SetVariable("DIPOLE_ALPHA_FF",ToString(helpd));

  helpd = reader.Get("DIPOLE_ALPHA_FI", 0.0, "FI dipole \\alpha_{max}", METHOD);
  rpa->gen.SetVariable("DIPOLE_ALPHA_FI",ToString(helpd));

  helpd = reader.Get("DIPOLE_ALPHA_IF", 0.0, "IF dipole \\alpha_{max}", METHOD);
  rpa->gen.SetVariable("DIPOLE_ALPHA_IF",ToString(helpd));

  helpd = reader.Get("DIPOLE_ALPHA_II", 0.0, "II dipole \\alpha_{max}", METHOD);
  rpa->gen.SetVariable("DIPOLE_ALPHA_II",ToString(helpd));


  helpd = reader.Get("DIPOLE_KAPPA", 2.0/3.0, "dipole \\kappa", METHOD);
  rpa->gen.SetVariable("DIPOLE_KAPPA",ToString(helpd));

  helpi = reader.Get("DIPOLE_NF_GSPLIT", Flavour(kf_jet).Size()/2, "dipole N_f", METHOD);
  rpa->gen.SetVariable("DIPOLE_NF_GSPLIT",ToString(helpi));

  helpd = reader.Get("DIPOLE_KT2MAX", sqr(rpa->gen.Ecms()), "dipole \\k_{T,max}^2", METHOD);
  rpa->gen.SetVariable("DIPOLE_KT2MAX",ToString(helpd));

  rpa->gen.SetVariable("USR_WGT_MODE",
		       ToString(reader.Get("USR_WGT_MODE",1)));
  rpa->gen.SetVariable("NLO_SMEAR_THRESHOLD",
		       ToString(reader.Get("NLO_SMEAR_THRESHOLD",0.0)));
  rpa->gen.SetVariable("NLO_SMEAR_POWER",
		       ToString(reader.Get("NLO_SMEAR_POWER",0.5)));

#ifdef USING__Threading
  helpi = reader.Get("COMIX_THREADS", 0, "number of threads", METHOD);
  if (helpi>0) {
    m_cts.resize(helpi);
    for (size_t i(0);i<m_cts.size();++i) {
      CDBG_ME_TID *tid(new CDBG_ME_TID());
      m_cts[i] = tid;
      pthread_cond_init(&tid->m_s_cnd,NULL);
      pthread_cond_init(&tid->m_t_cnd,NULL);
      pthread_mutex_init(&tid->m_s_mtx,NULL);
      pthread_mutex_init(&tid->m_t_mtx,NULL);
      pthread_mutex_lock(&tid->m_t_mtx);
      tid->m_s=1;
      int tec(0);
      if ((tec=pthread_create(&tid->m_id,NULL,&Amplitude::TCalcJL,(void*)tid)))
	THROW(fatal_error,"Cannot create thread "+ToString(i));
    }
  }
#endif
#ifdef USING__MPI
  if (MPI::COMM_WORLD.Get_rank()==0)
#endif
  MakeDir(rpa->gen.Variable("SHERPA_CPP_PATH")+"/Process",true);
  My_In_File::OpenDB
    (rpa->gen.Variable("SHERPA_CPP_PATH")+"/Process/Comix/");
  return true;
}

PHASIC::Process_Base *Comix::
InitializeProcess(const PHASIC::Process_Info &pi, bool add)
{
  if (p_model==NULL) return NULL;
  m_umprocs.push_back(std::vector<Single_Process*>());
  PHASIC::Process_Base *newxs(NULL);
  bool oneisgroup(pi.m_ii.IsGroup()||pi.m_fi.IsGroup());
  std::map<std::string,std::string> pmap;
  if (oneisgroup) {
    newxs = new Process_Group();
    newxs->SetGenerator(this);
    newxs->Init(pi,p_int->Beam(),p_int->ISR());
    newxs->Get<COMIX::Process_Base>()->SetModel(p_model);
    newxs->Get<COMIX::Process_Base>()->SetCTS(p_cts);
    if (!newxs->Get<Process_Group>()->Initialize(&pmap,&m_umprocs.back())) {
      msg_Debugging()<<METHOD<<"(): Init failed for '"
		     <<newxs->Name()<<"'\n";
      delete newxs;
      return NULL;
    }
    newxs->Integrator()->SetHelicityScheme(pi.m_hls);
    newxs->Get<COMIX::Process_Base>()->SetGPath(pi.m_gpath);
    My_In_File::ExecDB
      (rpa->gen.Variable("SHERPA_CPP_PATH")+"/Process/Comix/","begin");
    if (!newxs->Get<PHASIC::Process_Group>()->ConstructProcesses()) {
      My_In_File::ExecDB
	(rpa->gen.Variable("SHERPA_CPP_PATH")+"/Process/Comix/","commit");
      msg_Debugging()<<METHOD<<"(): Construct failed for '"
		     <<newxs->Name()<<"'\n";
      delete newxs;
      return NULL;
    }
    My_In_File::ExecDB
      (rpa->gen.Variable("SHERPA_CPP_PATH")+"/Process/Comix/","commit");
    msg_Tracking()<<"Initialized '"<<newxs->Name()<<"'\n";
  }
  else {
    newxs = new Single_Process();
    newxs->SetGenerator(this);
    newxs->Init(pi,p_int->Beam(),p_int->ISR());
    newxs->Integrator()->SetHelicityScheme(pi.m_hls);
    newxs->Get<COMIX::Process_Base>()->SetModel(p_model);
    newxs->Get<COMIX::Process_Base>()->SetCTS(p_cts);
    newxs->Get<COMIX::Process_Base>()->SetGPath(pi.m_gpath);
    My_In_File::ExecDB
      (rpa->gen.Variable("SHERPA_CPP_PATH")+"/Process/Comix/","begin");
    if (!newxs->Get<Single_Process>()->Initialize(&pmap,&m_umprocs.back())) {
      My_In_File::ExecDB
	(rpa->gen.Variable("SHERPA_CPP_PATH")+"/Process/Comix/","commit");
      msg_Debugging()<<METHOD<<"(): Init failed for '"
		     <<newxs->Name()<<"'\n";
      delete newxs;
      return NULL;
    }
    if (!newxs->Get<Single_Process>()->MapProcess())
      if (!msg_LevelIsTracking()) msg_Info()<<"."<<std::flush;
    My_In_File::ExecDB
      (rpa->gen.Variable("SHERPA_CPP_PATH")+"/Process/Comix/","commit");
  }
  if (add) Add(newxs,1);
  else m_rsprocs.push_back(newxs);
  return newxs;
}

int Comix::PerformTests()
{
  My_In_File::CloseDB
    (rpa->gen.Variable("SHERPA_CPP_PATH")+"/Process/Comix/");
  if (!Tests()) return 0;
  for (size_t i=0;i<m_rsprocs.size();++i)
    if (!m_rsprocs[i]->Get<COMIX::Process_Base>()->Tests()) return false;
  return 1;
}

bool Comix::NewLibraries()
{
  return false;
}

DECLARE_GETTER(Comix,"Comix",ME_Generator_Base,ME_Generator_Key);

ME_Generator_Base *ATOOLS::Getter
<ME_Generator_Base,ME_Generator_Key,Comix>::
operator()(const ME_Generator_Key &key) const
{
  return new Comix();
}

void ATOOLS::Getter<ME_Generator_Base,ME_Generator_Key,Comix>::
PrintInfo(std::ostream &str,const size_t width) const
{ 
  str<<"The Comix ME generator"; 
}
